import codecs
try:
    codecs.lookup('mbcs')
except LookupError:
    ascii = codecs.lookup('ascii')
    func = lambda name, enc=ascii: {True: enc}.get(name=='mbcs')
    codecs.register(func)

from setuptools import setup

setup(
    name='harvey2agility',
    version='0.0.1',
    description='Converts Harvey XML to Agility CSV',
    author='Blake Howell',
    author_email='sailpuff322@gmail.com',
    packages=['harvey2agility', ],
    entry_points={
        'console_scripts': ['harvey2agility=harvey2agility:main'],
    }
)
