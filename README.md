# harvey2agility
A command line utility to convert harvey XML export documents to CSV files for importing into Agility.

# How to build
## Windows Portable exe
* Install the requirements
`pip install -r requirements.txt`
* Build the executable
`pynist installer.cfg`
* Run the .exe in the /build dir
## Python
* python ./setup.py
