import argparse
import csv
import os

from xml.etree import ElementTree as ET


HEADERS = ['Quote Number', 'Line Number', 'Description', 'Quantity', 'Unit Price', 'Room Location', 'Dealer Price']


def parse(input_file, output_file):
    root = ET.parse(input_file)
    user_quote_number = root.find('userquotenumber').attrib['Value']
    line_item_masters = root.findall('.//lineitemmaster')
    if line_item_masters is None:
        print('Could not find any line items')
        exit(1)
    total_index = 0
    with open(output_file, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(HEADERS)
        for line_item_master in line_item_masters:
            line_item_master_number = line_item_master.find('linenumber').attrib['Value']
            line_items = line_item_master.findall('.//lineitem')
            for line_item in line_items:
                total_index += 1
                if not total_index % 10:
                    print('Writing line: {0}'.format(total_index))
                line_number = '-'.join([line_item_master_number, line_item.find('linenumber').attrib['Value'], ])
                description = line_item.find('description').text
                quantity = line_item.find('quantity').attrib['Value']
                unit_price = line_item.find('customerprice').attrib['Value']
                room_location = line_item.find('room').text
                dealer_price = line_item.find('dealerprice').attrib['Value']
                writer.writerow([user_quote_number, line_number, description, quantity, unit_price, room_location,
                                  dealer_price])
    print('Wrote {0} lines to the csv'.format(total_index))
    print('Finished parsing file')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    parser.add_argument('output')
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print('Input file: {0} does not exist'.format(args.input))
        exit(1)
    parse(args.input, args.output)


if __name__ == '__main__':
    main()
